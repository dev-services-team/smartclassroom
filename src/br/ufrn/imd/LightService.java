package br.ufrn.imd;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import context.arch.comm.DataObject;
import context.arch.service.Service;
import context.arch.service.helper.FunctionDescription;
import context.arch.service.helper.FunctionDescriptions;
import context.arch.service.helper.ServiceInput;
import context.arch.widget.Widget;


public class LightService extends Service {
	
	
	public JLabel lightLabel;

	@SuppressWarnings("serial")
	public LightService(final Widget widget) {
		super(widget, "LightService", new FunctionDescriptions() {
					{
						add(new FunctionDescription(
								"lightControl", 
								"Sets the light level of the lamp", 
								widget.getNonConstantAttributes()));
					}
				});
		
		
		lightLabel = new JLabel("Desligado") {{
			setHorizontalAlignment(JLabel.RIGHT);
			setBorder(BorderFactory.createEtchedBorder());
			setFont(new Font("Arial", Font.BOLD, 20));
			setOpaque(true); 
		}};
	}

	@Override
	public DataObject execute(ServiceInput serviceInput) {
		
		int light = serviceInput.getInput().getAttributeValue("light");
		
		if (light == 0) {
			lightLabel.setBackground(Color.black);
			lightLabel.setText("Desligado");
		} else {
			lightLabel.setBackground(Color.yellow);
			lightLabel.setText("Ligado");
		}
		
		return new DataObject();
	}

}
