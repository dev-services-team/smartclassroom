package br.ufrn.imd;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import context.arch.comm.DataObject;
import context.arch.service.Service;
import context.arch.service.helper.FunctionDescription;
import context.arch.service.helper.FunctionDescriptions;
import context.arch.service.helper.ServiceInput;
import context.arch.widget.Widget;


public class ArService extends Service {
	
	
	public JLabel arLabel;

	@SuppressWarnings("serial")	
	public ArService(final Widget widget) {
		
		super(widget, "ArService", new FunctionDescriptions() {
					{
						add(new FunctionDescription(
								"arControl", 
								"Set ar-conditioning on or off", 
								widget.getNonConstantAttributes()));
					}
				});
		
		arLabel = new JLabel("AR") {{
			setHorizontalAlignment(JLabel.RIGHT);
			setBorder(BorderFactory.createEtchedBorder());
			setOpaque(true); 
			setBackground(Color.gray);
			setFont(new Font("Arial", Font.BOLD, 20));
		}};
	}

	@Override
	public DataObject execute(ServiceInput serviceInput) {
		
		
		int ar = serviceInput.getInput().getAttributeValue("ar");
		if (ar == 0) {
			arLabel.setText("Desligado");
		} else {
			arLabel.setText(String.valueOf(ar) + "�C");
		}
				
		
		return new DataObject(); 
	}

}