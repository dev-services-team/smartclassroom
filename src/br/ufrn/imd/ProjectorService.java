package br.ufrn.imd;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import context.arch.comm.DataObject;
import context.arch.service.Service;
import context.arch.service.helper.FunctionDescription;
import context.arch.service.helper.FunctionDescriptions;
import context.arch.service.helper.ServiceInput;
import context.arch.widget.Widget;


public class ProjectorService extends Service {
	
	
	public JLabel projectorLabel;

	@SuppressWarnings("serial")	
	public ProjectorService(final Widget widget) {
		
		super(widget, "ProjectorService", new FunctionDescriptions() {
					{
						add(new FunctionDescription(
								"projectorControl", 
								"Set projector on or off", 
								widget.getNonConstantAttributes()));
					}
				});
		
		projectorLabel = new JLabel("Desligado") {{
			setHorizontalAlignment(JLabel.RIGHT);
			setBorder(BorderFactory.createEtchedBorder());
			setFont(new Font("Arial", Font.BOLD, 20));
			setOpaque(true); 
		}};
	}

	@Override
	public DataObject execute(ServiceInput serviceInput) {
		
		
		int projector = serviceInput.getInput().getAttributeValue("projector");
		
		if (projector == 0) {
			projectorLabel.setBackground(Color.red);
			projectorLabel.setText("Desligado");
			
		} else {
			projectorLabel.setBackground(Color.green);
			projectorLabel.setText("Ligado");
		}
		
		return new DataObject(); 
	}

}