package br.ufrn.imd;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import context.arch.discoverer.Discoverer;
import context.arch.enactor.Enactor;
import context.arch.enactor.EnactorXmlParser;
import context.arch.widget.Widget;
import context.arch.widget.WidgetXmlParser;

public class ClassRoom {
	
	protected Widget roomWidget;
	protected Widget lightWidget;
	protected Widget projectorWidget;
	protected Widget computerWidget;
	protected Widget arWidget;
	
	protected Enactor lightEnactor;
	protected Enactor projectorEnactor;
	protected Enactor arEnactor;
	protected Enactor computerEnactor;
	
	protected LightService lightService;
	protected ProjectorService projectorService;
	protected ArService arService;
	protected ComputerService computerService;
	
	protected SmartRoomUI ui;
	
	public ClassRoom() {
		
		super();
		
		
		roomWidget = WidgetXmlParser.createWidget("xml/room-widget.xml");
		lightWidget = WidgetXmlParser.createWidget("xml/light-widget.xml");
		projectorWidget = WidgetXmlParser.createWidget("xml/projector-widget.xml");
		arWidget = WidgetXmlParser.createWidget("xml/ar-widget.xml");
		computerWidget = WidgetXmlParser.createWidget("xml/computer-widget.xml");
		
		lightService = new LightService(lightWidget);
		projectorService = new ProjectorService(projectorWidget);
		arService = new ArService(arWidget);
		computerService = new ComputerService(computerWidget);
		
		lightWidget.addService(lightService);
		projectorWidget.addService(projectorService);
		arWidget.addService(arService);
		computerWidget.addService(computerService);
		
		lightEnactor = EnactorXmlParser.createEnactor("xml/light-enactor.xml");
		projectorEnactor = EnactorXmlParser.createEnactor("xml/projector-enactor.xml");
		arEnactor = EnactorXmlParser.createEnactor("xml/ar-enactor.xml");
		computerEnactor = EnactorXmlParser.createEnactor("xml/computer-enactor.xml");
		
		ui = new SmartRoomUI(lightService.lightLabel, projectorService.projectorLabel, arService.arLabel, computerService.computerLabel); 
	}
	
	
	@SuppressWarnings("serial")
	public class SmartRoomUI extends JPanel {
		
		private JComboBox<String> professorCombobox;
		
		private float fontSize = 20f;
		
		public SmartRoomUI(JLabel lightLabel, JLabel projectorLabel, JLabel arLabel, JLabel computerLabel) {		
			
			setLayout(new GridLayout(6, 2));
			
			add(new JLabel("Professor:") {{ setFont(getFont().deriveFont(fontSize)); }});
			
			add(professorCombobox = new JComboBox<String>());
			professorCombobox.addItem("Sala Vazia");
			professorCombobox.addItem("Thais");
			professorCombobox.addItem("Fred");
			professorCombobox.addItem("Francisco");
			professorCombobox.addItem("Itamir");
			
			professorCombobox.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					String professor = (String) professorCombobox.getSelectedItem();
					roomWidget.updateData("professor", professor);
								
				}
			});
			
	
			add(new JLabel("Luz:") {{ setFont(getFont().deriveFont(fontSize)); }});
			add(lightLabel);
			
			
			add(new JLabel("Ar:") {{ setFont(getFont().deriveFont(fontSize)); }});
			add (arLabel);
			
			add(new JLabel("Computador:") {{ setFont(getFont().deriveFont(fontSize)); }});
			add(computerLabel);
			
			
			add(new JLabel("Projetor:") {{ setFont(getFont().deriveFont(fontSize)); }});
			add(projectorLabel);
			
			
//			int presence = (Integer) presenceSpinner.getValue();
//			roomWidget.updateData("presence", 0);
		}
		
	}
	
	public static void main(String[] args) {
		
		Discoverer.start();
		
		ClassRoom app = new ClassRoom();
		JFrame frame = new JFrame("SmartClassRoom");
		frame.add(app.ui);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(1000, 300));
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

}
