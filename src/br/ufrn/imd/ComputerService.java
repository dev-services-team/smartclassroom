package br.ufrn.imd;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import context.arch.comm.DataObject;
import context.arch.service.Service;
import context.arch.service.helper.FunctionDescription;
import context.arch.service.helper.FunctionDescriptions;
import context.arch.service.helper.ServiceInput;
import context.arch.widget.Widget;


public class ComputerService extends Service {
	
	
	public JLabel computerLabel;

	@SuppressWarnings("serial")	
	public ComputerService(final Widget widget) {
		
		super(widget, "ComputerService", new FunctionDescriptions() {
					{
						add(new FunctionDescription(
								"computerControl", 
								"Set computer on or off", 
								widget.getNonConstantAttributes()));
					}
				});
		
		computerLabel = new JLabel("COMPUTADOR") {{
			setHorizontalAlignment(JLabel.RIGHT);
			setBorder(BorderFactory.createEtchedBorder());
			setFont(new Font("Arial", Font.BOLD, 20));
			setOpaque(true); 
		}};
	}

	@Override
	public DataObject execute(ServiceInput serviceInput) {
		
		
		int computer = serviceInput.getInput().getAttributeValue("computer");
		
		
		if (computer == 0) {
			computerLabel.setBackground(Color.gray);
			computerLabel.setText("Desligado");
		} else if (computer == 1){
			computerLabel.setBackground(Color.pink);
			computerLabel.setText("Slides T�pico Avan�ados - Thais");
		} else if (computer == 2){
			computerLabel.setBackground(Color.cyan);
			computerLabel.setText("Slides T�picos Avan�ados - Fred");
		} else if (computer == 3){
			computerLabel.setBackground(Color.orange);
			computerLabel.setText("Slides T�picos Avan�ados - Francisco");
		} else if (computer == 4) {
			computerLabel.setBackground(Color.magenta);
			computerLabel.setText("Slides T�picos Avan�ados - Itamir");
		}
		
		return new DataObject(); 
	}

}